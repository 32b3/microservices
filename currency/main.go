package main

import (
	"fmt"
	"net"
	"os"

	"gitlab.com/32b3/microservices/currency/data"

	protos "gitlab.com/32b3/microservices/currency/protos/currency"
	"gitlab.com/32b3/microservices/currency/server"

	"github.com/hashicorp/go-hclog"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	log := hclog.Default()
	rates, err := data.NewRates(log)
	if err != nil {
		log.Error("Unable to generate rates", "error", err)
		os.Exit(1)
	}
	gs := grpc.NewServer()

	c := server.NewCurrency(rates, log)
	protos.RegisterCurrencyServer(gs, c)

	reflection.Register(gs)

	l, err := net.Listen("tcp", fmt.Sprintf(":%d", 9092))
	if err != nil {
		log.Error("Unable to create listener", "error", err)
		os.Exit(1)
	}
	log.Info("Listener listening on port 9092")
	gs.Serve(l)

}
