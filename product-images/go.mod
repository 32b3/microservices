module gitlab.com/32b3/microservices/product-images

go 1.15

require (
	github.com/fatih/color v1.9.0 // indirect
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
	github.com/hashicorp/go-hclog v0.14.1
	github.com/mattn/go-colorable v0.1.8 // indirect
	github.com/nicholasjackson/env v0.6.0
	github.com/stretchr/testify v1.3.0
	golang.org/x/sys v0.0.0-20201028094953-708e7fb298ac // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1
)
